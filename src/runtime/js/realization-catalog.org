#+TITLE: Realization Catalog
#+AUTHOR: VLEAD
#+DATE: [2018-11-29 Thu]
#+SETUPFILE: ../../org-templates/level-2.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Introduction
This document captures the realization catalog of bubble sort
Experiment.

* Realization Catalog
#+NAME: realization-catalog
#+BEGIN_SRC js
var realizationCatalog = { 
  // Image Artefacts
  "Naive Visualization": {
    "resource_type": "Image",
    "url": "/build/code/static/img/NaiveExample.png",
    "height": "40%",
    "width": "60%"
  },
  "KMP Algorithm Visualization": {
    "resource_type": "Image",
    "url": "/build/code/static/img/kmpExample.png",
    "width": "80%",
    "height": "100%"
  },
  "LPS Visualization": {
    "resource_type": "Image",
    "url": "/build/code/static/img/introLpsImage.png",
    "width": "30%",
    "height": "40%"
  },
  "Preprocessing Visualization": {
    "resource_type": "Image",
    "url": "/build/code/static/img/preprocessingImage.png",
    "width": "30%",
    "height": "55%"
  },

  // Video Artefacts
  "Introduction Video": {
    "resource_type": "video",
    "url": "https://www.youtube.com/embed/bQmLvZnFUA0"
  },

  "Optimized KMP Concept": {
    "resource_type": "video",
    "url": "https://www.youtube.com/embed/AVntfZSptew"
  },

  "Naive String Searching Concept": {
    "resource_type": "video",
    "url": "https://www.youtube.com/embed/3t3u0EUerLI"
  },

  "Analysis of KMP Algorithm": {
    "resource_type": "video",
    "url": "https://www.youtube.com/embed/StPHqA7-l2M"
  },
  
  // HTML Artefacts
  "KMP String Searching Demo": {
    "reqs_satisfied": "Provides an artefact to show the opti bubble sort algo",
    "resource_type": "html",
    "url": "/build/code/runtime/html/kmpDemo.html"
  },
  "KMP String Searching Exercise": {
    "reqs_satisfied": "Provides an artefact as exercise for kmp Algorithm",
    "resource_type": "html",
    "url": "/build/code/runtime/html/kmpExercise.html"
  },
  "KMP String Searching Practice": {
    "reqs_satisfied": "Provides an artefact as exercise for kmp Algorithm",
    "resource_type": "html",
    "url": "/build/code/runtime/html/kmpPractice.html"
  },
  "Naive String Searching Demo": {
    "reqs_satisfied": "Provides an artefact to show the split operation",
    "resource_type": "html",
    "url": "/build/code/runtime/html/naiveDemo.html"
  },
  "Preprocessing Demo": {
    "resource_type": "html",
    "url": "/build/code/runtime/html/preprocessingDemo.html"
  }
};
#+END_SRC

* Tangle
#+BEGIN_SRC javascript :tangle realization.js :eval no :noweb yes
<<realization-catalog>>
#+END_SRC
