#+TITLE: Demonstration artefact HTML for Preprocessing for KMP
#+AUTHOR:VLEAD
#+DATE: 
#+SETUPFILE: ./org-templates/level-0.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Introduction
  This document builds the =KMP Algorithm= interactive
  artefact(HTML).

* Features of the artefact
+ Artefact provides demo for KMP Algorithm
+ User can see a randomly generated array of size 10 in the
  beginning. Size of the array is fixed.
+ User can click the =Reset= button to generate a new array
  at any time.
+ User shall click the =start= button to start the demo.
+ User can slide use a =slider= to adjust speed of the demo
  as per his/her convenience.
+ User can pause the demo by clicking on the =pause= button.
+ User can pause and then click on =next= button to manually
  view the demo at his/her own speed.

* HTML Framework of KMP Algorithm
** Head Section Elements
Title, meta tags and all the links related to Js, CSS of
this artefact comes under this section.
#+NAME: head-section-elems
#+BEGIN_SRC html
  <title>KMP Demo</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://ds1-iiith.vlabs.ac.in/exp-common-css/common-styles.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css">
  <link rel="stylesheet" href="../css/demoUI.css">
#+END_SRC

** Body Section Elements
*** Instruction box
This is an instruction box, in which the instructions needed
for the user to perform the experiments are displayed in a
form of list.
#+NAME: instructionBox
#+BEGIN_SRC html
<div class="instruction-box">
  <button class="collapsible">Instructions</button>
  <div class="content">
    <ul>
      <li>Click on the <b>Setup</b> button to start the
        demo.</li>
      <li>Click on the <b>Next</b> button to move the demo one
        match at a time</li>
      <li>You may click on the <b>Setup</b> button after
        enterring a text of your own to have a thorough
        understanding of the algorithm.</li>
      <li>Feel free to play around to understand the
        concept to its full depth.</li>
    </ul>
  </div>
</div>

#+END_SRC

*** Demo Pane
This div has the actual visualisation elements of the
experiment.  Positioning this will change the position of
the animation pane that shows.
#+NAME: demoPane
#+BEGIN_SRC html
<div class="demoPane">
  <table id="patternString">
    <tr id="patternStringRow"></tr>
    <tr id="values"></tr>
  </table>
</div>

#+END_SRC

*** Comments Box
This displays the insight into the various steps taken in
the experiment.
#+NAME: comments
#+BEGIN_SRC html
<div class="comment-box">
  <b>Observations</b><br>
  <p id="comment"></p>
</div>

#+END_SRC

*** Input box
This contains all the input text fields and buttons needed
for the experiment.
#+NAME: allInput
#+BEGIN_SRC html
<div class="allInput">
  <label>Pattern to create the LPS Array :</label>
  <input type="text" class="text-box" id="patternInput" value="AABAACAABAC" maxlength="10"><br><br>
  <input type="submit" class="button-input" value="Next" id="Next">
  <input type="submit" class="button-input" value="Setup" id="Setup">
</div>

#+END_SRC

** JS Imports
#+NAME: scripts
#+BEGIN_SRC html
  <script
      src="https://code.jquery.com/jquery-3.4.1.js"
      integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
      crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js"></script>
  <script src="../js/preprocessingDemoJS.js"></script>
  <script src="../js/demoUI_Preprocessing.js"></script>
  <script>
      function handlers() {
         document.getElementById("Next").onclick = function() { next();};
         document.getElementById("Setup").onclick = function() { setup(); };
         $("input[id='Next']").attr("disabled", true);
      }
  </script>
  <script src="https://ds1-iiith.vlabs.ac.in/exp-common-js/instruction-box.js"></script>
#+END_SRC

* Tangle
#+BEGIN_SRC html :tangle preprocessingDemo.html :eval no :noweb yes
<!DOCTYPE html>
<html lang="en">
  <head>
    <<head-section-elems>>
  </head>
  <body onload="handlers();">
    <<instructionBox>>
    <div id="wrapper">
      <<demoPane>>
      <div id="comment-wrapper">
         <<comments>>
      </div>
      <<allInput>>
    </div>
    <<scripts>>
  </body>
</html>
#+END_SRC
